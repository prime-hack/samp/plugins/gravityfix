import core.sys.windows.windows, core.sys.windows.dll;
import std.concurrency : spawn, yield;
import core.runtime;

import llmo;

extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID) {
	final switch (ulReason) {
	case DLL_PROCESS_ATTACH:
		Runtime.initialize();
		spawn({
			while (*cast(uint*)0xC8D4C0 < 9)
				yield();
			init();
		});
		dll_process_attach(hInstance, true);
		break;
	case DLL_PROCESS_DETACH:
		uninit();
		Runtime.terminate();
		dll_process_detach(hInstance, true);
		break;
	case DLL_THREAD_ATTACH:
		dll_thread_attach(true, true);
		break;
	case DLL_THREAD_DETACH:
		dll_thread_detach(true, true);
		break;
	}
	return true;
}

void hook_collision() pure @nogc {
	asm {
		naked;
		pop EAX;
		test byte ptr[ESI + 0x36], 1;
		jnz ORIGINAL_CODE;
		test byte ptr[ESI + 0x36], 2;
		jnz END_HOOK;

	ORIGINAL_CODE:
		test byte ptr[ESI + 0x1C], 1;
		jnz END_HOOK;
		mov EAX, 0x543093; // skip gravity

	END_HOOK:
		push EAX;
		ret;
	}
}

void hook_gravity() pure @nogc {
	asm {
		naked;
		pop EAX;
		test byte ptr[ESI + 0x36], 1;
		jnz END_HOOK_GRAVITY;
		test byte ptr[ESI + 0x36], 2;
		jz END_HOOK_GRAVITY;
		test byte ptr[ESI + 0x1C], 1;
		jz END_HOOK_GRAVITY;
		mov EAX, 0x542FF1; // ignore gravity flag

	END_HOOK_GRAVITY:
		push EAX;
		mov EAX, [ESI + 0x40];
		test AL, 2;
		ret;
	}
}

/// Uninitialize plugin: restore original code
void uninit() {
	writeMemory(0x54307B, [0xF6, 0x46, 0x1C, 0x01, 0x74, 0x12], MemorySafe.safe);
	writeMemory(0x542FE6, [0x8B, 0x46, 0x40, 0xA8, 0x02], MemorySafe.safe);
}

/// Initialize plugin: set hacks
void init() {
	simpleCallHook(0x54307B, &hook_collision, 6);
	simpleCallHook(0x542FE6, &hook_gravity, 5);
}
